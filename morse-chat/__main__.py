from os import system, name
import yaml

# Get the config data
with open('morse-chat/config.yaml', 'r') as yaml_file:
    cfg = yaml.load(yaml_file, Loader=yaml.FullLoader)
op_name = cfg['operator']['name']
op_call = cfg['operator']['call']
qth = cfg['operator']['qth']

# Get the char to morse data
with open('morse-chat/char2morse.yaml', 'r') as yaml_file:
    c2m = yaml.load(yaml_file, Loader=yaml.FullLoader)


def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')
    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def main():
    clear()
    print('Welcome ' + op_name + '. Your call sign is ' + op_call + ' and you are in ' + qth + '.')
    print('Type your message + Enter to transmit. To exit, type arcl + Enter to close (ar=OUT cl=CLOSE).')
    while True:
        ip = input(cfg['operator']['call'] + ': ')
        if ip == 'arcl':
            bk = 1
            tx_msg = '73 DE ' + op_call + ' AR CL'
        else:
            bk = 0
            tx_msg = ip + ' DE ' + op_call + ' K'
        print('Text: ' + tx_msg)
        m_msg = ''
        for char in list(tx_msg):
            m_msg += c2m.get(char.lower(), '   ')
            m_msg += ' '
        print('Morse: ' + m_msg)
        if bk == 1:
            break


if __name__ == "__main__":
    main()
