import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="morse-chat",
    version="2019.0.1",
    author="Jeff Ussing",
    author_email="jeffussing@gmail.com",
    description="A simple chat client. Read and write in plain text; transmit and receive in morse code.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jeffussing/morse-chat",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
              'console_scripts': [
                  'morse-chat = morse-chat.__main__:main'
              ]
          },
)
