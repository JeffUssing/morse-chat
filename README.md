# Morse Chat

Morse Chat is simple chat client that can read and write in plain text and transmit and receive in morse code.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install Morse Chat.

```bash
pip install morse-chat
```

## Usage

```bash
python morse-chat
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html)
